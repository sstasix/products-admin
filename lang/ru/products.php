<?php

declare(strict_types=1);

return [
    'navigation_index_link' => 'Продукты',
    'navigation_create_link' => 'Добавить',

    'index_title' => 'Список продуктов',
    'empty_table_message' => 'Нет продуктов',

    'create_page_title' => 'Добавить продукт',
    'article_number' => 'Артикул',
    'name' => 'Название',
    'properties_title' => 'Свойства продукта',
    'add_property_link' => 'Добавить',
    'status' => 'Статус',

    'create_button_text' => 'Создать',
    'update_button_text' => 'Сохранить',
    'destroy_confirm_message' => 'Удалить?',

    'mail_product_created_subject' => 'Добавлен новый продект',
];
