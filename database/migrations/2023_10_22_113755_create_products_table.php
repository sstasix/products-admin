<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('article_number', 255)
                ->unique()
                ->comment('Артикул продукта');
            $table->string('name', 255)
                ->comment('Название продукта');
            $table->enum('status', ['available', 'unavailable'])
                ->comment('Статус продукта');
            $table->jsonb('data')
                ->comment('Свойства продукта')
                ->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
