<?php

namespace Database\Factories;

use App\Enums\ProductStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => fake()->sentence(5),
            'article_number' => fake()->unique()->word().fake()->randomNumber(3),
            'status' => fake()->randomElement(ProductStatus::cases()),
            'data' => $this->generateProperties(),
        ];
    }

    protected function generateProperties(): array
    {
        $data = [];
        foreach (range(1, random_int(1, 5)) as $index) {
            $data[fake()->unique()->word()] = fake()->randomElement([
                fake()->word(),
                random_int(1, 100),
            ]);
        }

        return $data;
    }
}
