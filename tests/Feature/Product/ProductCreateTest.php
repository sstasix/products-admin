<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Database\Factories\ProductFactory;
use Illuminate\Pagination\Paginator;

test('not authenticated user can not see create page', function () {
    $response = $this->get(route('products.create'));

    $response->assertRedirect();
});

test('authenticated user can see create page', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.create'));

    $response->assertOk();
    $response->assertViewIs('products.create');
});

test('create product form is displayed', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.create'));

    $response->assertSeeText(__('products.create_page_title'));
    $response->assertSeeText(__('products.create_button_text'));
    $response->assertSee(route('products.store'));
});
