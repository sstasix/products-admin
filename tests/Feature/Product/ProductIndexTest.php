<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Database\Factories\ProductFactory;
use Illuminate\Pagination\Paginator;

test('can see index page', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.index'));

    $response->assertOk();
    $response->assertViewIs('products.index');
});

test('not authenticated user can not see index page', function () {
    $response = $this->get(route('products.index'));

    $response->assertRedirect(route('login'));
});

test('empty table message is displayed', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.index'));

    $response->assertSeeText(__('products.empty_table_message'));
});

test('product article number and buttons is displayed', function () {
    $product = Product::factory()->create();
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.index'));

    $response->assertSeeText($product->article_number);
    $response->assertSee(route('products.edit', $product));
    $response->assertSee(route('products.destroy', $product));
});
