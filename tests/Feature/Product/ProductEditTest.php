<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;

test('non authenticated user can not see edit page', function () {
    $product = Product::factory()->create();

    $response = $this->get(route('products.edit', $product));

    $response->assertRedirect();
});

test('authenticated user can see edit page', function () {
    $user = User::factory()->create();
    $product = Product::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.edit', $product));

    $response->assertOk();
    $response->assertViewIs('products.edit');
    $response->assertSeeText(__('products.update_button_text'));
});

test('user can not edit deleted product', function () {
    $user = User::factory()->create();
    $product = Product::factory()->create();
    $product->delete();

    $response = $this
        ->actingAs($user)
        ->get(route('products.edit', $product));

    $response->assertNotFound();
});

test('user can not edit undefined product', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get(route('products.edit', 1));

    $response->assertNotFound();
});
