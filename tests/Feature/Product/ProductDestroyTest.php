<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;

test('can destroy successfully', function () {
    $user = User::factory()->create();
    $product = Product::factory()->createOne();

    $response = $this
        ->actingAs($user)
        ->delete(route('products.destroy', $product));

    $this->assertSoftDeleted((new Product())->getTable(), [
        'name' => $product->name,
        'article_number' => $product->article_number,
        'status' => $product->status->value,
    ]);
    $response->assertRedirect(route('products.index'));
});

test('not authenticated user can not destroy product', function () {
    $product = Product::factory()->createOne();

    $response = $this
        ->delete(route('products.destroy', $product));

    $response->assertRedirect(route('login'));
});
