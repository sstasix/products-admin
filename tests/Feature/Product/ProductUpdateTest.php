<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;

test('can update data successfully', function () {
    $user = User::factory()->create();
    $product = Product::factory()->createOne();
    $newProduct = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->put(route('products.update', $product), [
            'name' => $newProduct->name,
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    $this->assertDatabaseHas((new Product())->getTable(), [
        'name' => $newProduct->name,
        'article_number' => $product->article_number,
        'status' => $product->status->value,
    ]);
    $response->assertRedirect(route('products.index'));
});

test('not authenticated user can not send request', function () {
    $product = Product::factory()->createOne();

    // Отправляем запрос на обновление без авторизации
    $response = $this
        ->put(route('products.update', $product), [
            'name' => $product->name,
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    $response->assertRedirect(route('login'));
});

test('non admin user can not update article number', function () {
    $user = User::factory()->create();
    $product = Product::factory()->createOne();
    $newProduct = Product::factory()->makeOne();

    // Обновляем артикул на новый
    $response = $this
        ->actingAs($user)
        ->put(route('products.update', $product), [
            'name' => $product->name,
            'article_number' => $newProduct->article_number,
            'status' => $product->status->value,
        ]);

    // Обновление прошло без ошибок
    $response->assertRedirect(route('products.index'));
    // В БД после обновления остался артикул старого продукта
    $this->assertDatabaseHas((new Product())->getTable(), [
        'name' => $product->name,
        'article_number' => $product->article_number,
        'status' => $product->status->value,
    ]);
});

test('admin user can update article number', function () {
    $adminEmailFromConfig = head(config('products.admins'));
    $admin = User::factory()->create(['email' => $adminEmailFromConfig]);
    $product = Product::factory()->createOne();
    $newProduct = Product::factory()->makeOne();

    // Обновляем артикул на новый
    $response = $this
        ->actingAs($admin)
        ->put(route('products.update', $product), [
            'name' => $product->name,
            'article_number' => $newProduct->article_number,
            'status' => $product->status->value,
        ]);

    // Обновление прошло без ошибок
    $response->assertRedirect(route('products.index'));
    // В БД после обновления появился артикул нового продукта
    $this->assertDatabaseHas((new Product())->getTable(), [
        'name' => $product->name,
        'article_number' => $newProduct->article_number,
        'status' => $product->status->value,
    ]);
});

test('admin user can update with old article number', function () {
    $adminEmailFromConfig = head(config('products.admins'));
    $admin = User::factory()->create(['email' => $adminEmailFromConfig]);
    $product = Product::factory()->createOne();
    $newProduct = Product::factory()->makeOne();

    // Обновляем артикул на новый
    $response = $this
        ->actingAs($admin)
        ->put(route('products.update', $product), [
            'name' => $newProduct->name,
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    // Обновление прошло без ошибок
    $response->assertRedirect(route('products.index'));
    // В БД после обновления появился артикул нового продукта
    $this->assertDatabaseHas((new Product())->getTable(), [
        'name' => $newProduct->name,
        'article_number' => $product->article_number,
        'status' => $product->status->value,
    ]);
});
