<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use App\Notifications\ProductCreatedNotification;
use Illuminate\Support\Facades\Notification;

test('can store data successfully', function () {
    // Создаем пользователя для отправки уведомлений
    User::factory()->create(['email' => head(config('products.admins'))]);
    $user = User::factory()->create();
    $product = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $product->name,
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    $this->assertDatabaseHas((new Product())->getTable(), [
        'name' => $product->name,
        'article_number' => $product->article_number,
        'status' => $product->status->value,
    ]);
    $response->assertRedirect(route('products.index'));
});

test('notification is sent successfully', function () {
    Notification::fake();
    // Создаем пользователя для отправки уведомлений
    User::factory()->create(['email' => head(config('products.admins'))]);
    $user = User::factory()->create();
    $product = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $product->name,
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    Notification::assertSentTo(
        [User::where('email', head(config('products.admins')))->first()],
        ProductCreatedNotification::class
    );
});

test('not authenticated user can not send request', function () {
    $product = Product::factory()->makeOne();

    $response = $this
        ->post(route('products.store'), [
            'name' => $product->name,
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    $response->assertRedirect(route('login'));
});

test('can not store with empty name', function () {
    $user = User::factory()->create();
    $product = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => '',
            'article_number' => $product->article_number,
            'status' => $product->status->value,
        ]);

    $response->assertInvalid(['name']);
    $this->assertDatabaseMissing((new Product())->getTable(), [
        'article_number' => $product->article_number,
    ]);
});

test('can not store with empty article number', function () {
    $user = User::factory()->create();
    $product = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $product->name,
            'article_number' => '',
            'status' => $product->status->value,
        ]);

    $response->assertInvalid(['article_number']);
    $this->assertDatabaseMissing((new Product())->getTable(), [
        'name' => $product->name,
    ]);
});

test('can not store with exists article number', function () {
    $user = User::factory()->create();
    $firstProduct = Product::factory()->makeOne(['article_number' => 'article1']);

    $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $firstProduct->name,
            'article_number' => $firstProduct->article_number,
            'status' => $firstProduct->status->value,
        ]);

    $this->assertDatabaseHas((new Product())->getTable(), [
        'name' => $firstProduct->name,
        'article_number' => $firstProduct->article_number,
        'status' => $firstProduct->status->value,
    ]);

    $secondProduct = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $secondProduct->name,
            'article_number' => 'article1',
            'status' => $secondProduct->status->value,
        ]);

    $response->assertInvalid(['article_number']);
    $this->assertDatabaseMissing((new Product())->getTable(), [
        'name' => $secondProduct->name,
    ]);
});

test('can not store with empty status', function () {
    $user = User::factory()->create();
    $product = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $product->name,
            'article_number' => $product->article_number,
            'status' => '',
        ]);

    $response->assertInvalid(['status']);
    $this->assertDatabaseMissing((new Product())->getTable(), [
        'article_number' => $product->article_number,
    ]);
});

test('can not store with invalid status', function () {
    $user = User::factory()->create();
    $product = Product::factory()->makeOne();

    $response = $this
        ->actingAs($user)
        ->post(route('products.store'), [
            'name' => $product->name,
            'article_number' => $product->article_number,
            'status' => 'abc',
        ]);

    $response->assertInvalid(['status']);
    $this->assertDatabaseMissing((new Product())->getTable(), [
        'article_number' => $product->article_number,
    ]);
});
