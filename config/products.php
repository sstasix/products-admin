<?php

return [

    'admins' => [
        'admin@products.com',
    ],

    'notify_emails' => [
        'notify@products.com',
    ],

];
