@php use App\Enums\ProductStatus; @endphp
<section>
    <form method="post"
          action="@if(request()->routeIs('products.create')) {{ route('products.store') }} @else {{ route('products.update', $product) }} @endif"
          class="mt-10">
        @csrf

        @if(request()->routeIs('products.edit'))
            @method('PUT')
        @endif

        <div>
            <x-input-label
                for="name"
                :value="__('products.name')"
                class="mt-2"
            />
            <x-text-input
                id="name"
                name="name"
                class="mt-1 block w-1/3"
                value="{{ old('name') ?? $product->name ?? '' }}"
                required
            />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>

        <div>
            <x-input-label
                for="article_number"
                :value="__('products.article_number')"
                class="mt-2"
            />
                <x-text-input
                    id="article_number"
                    name="article_number"
                    class="mt-1 block w-1/3"
                    value="{{ old('article_number') ?? $product->article_number ?? '' }}"
                    required
                    :readonly="request()->routeIs('products.edit') && auth()->user()->is_admin === false"
                />
            <x-input-error :messages="$errors->get('article_number')" class="mt-2" />
        </div>

        <div class="mt-4">{{ __('products.properties_title') }}</div>
        @isset($product->data)
            @foreach($product->data as $property => $value)
                <div>
                    <x-text-input id="properties[]" name="properties[]" class="mt-1 w-1/4" value="{{ $property }}" required/>
                    <x-text-input id="values[]" name="values[]" class="mt-1 w-1/4" value="{{ $value }}" required/>
                    <svg onclick="parentElement.remove()"
                         class="h-6 w-6 text-red-500 inline-flex cursor-pointer"
                         width="24"
                         height="24"
                         viewBox="0 0 24 24"
                         stroke-width="1"
                         stroke="currentColor"
                         fill="none"
                         stroke-linecap="round"
                         stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z"/>
                        <line x1="4" y1="7" x2="20" y2="7" />
                        <line x1="10" y1="11" x2="10" y2="17" />
                        <line x1="14" y1="11" x2="14" y2="17" />
                        <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                        <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                    </svg>
                </div>
            @endforeach
        @endisset
        <div id="properties"></div>
        <div><span onclick="addProperty()" class="cursor-pointer underline text-sm text-blue-600">{{ __('products.add_property_link') }}</span></div>

        <div>
            <x-input-label for="status" :value="__('products.status')" class="mt-2"/>
            <select name="status" id="status" class="mt-1 block w-1/3 border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm">
                @foreach(ProductStatus::cases() as $status)
                    <option value="{{ $status }}" @selected((old('status') ?? $product->status->value ?? '') === $status->value)>
                        {{ $status->getLabelText() }}
                    </option>
                @endforeach
            </select>
            <x-input-error :messages="$errors->get('status')" class="mt-2"/>
        </div>

        <div class="flex items-center gap-4 mt-5">
            <x-primary-button>@if(request()->routeIs('products.create'))
                    {{ __('products.create_button_text') }}
                @else
                    {{ __('products.update_button_text') }}
                @endif</x-primary-button>
        </div>
    </form>
    <script>
        function addProperty() {
            let html = `
                    <div>
                        <x-text-input id="properties[]" name="properties[]" class="mt-1 w-1/4" value="" required/>
                        <x-text-input id="values[]" name="values[]" class="mt-1 w-1/4" value="" required/>
                        <svg onclick="parentElement.remove()"
                            class="h-6 w-6 text-red-500 inline-flex cursor-pointer"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            stroke-width="1"
                            stroke="currentColor"
                            fill="none"
                            stroke-linecap="round"
                            stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z"/>
                        <line x1="4" y1="7" x2="20" y2="7" />
                        <line x1="10" y1="11" x2="10" y2="17" />
                        <line x1="14" y1="11" x2="14" y2="17" />
                        <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                        <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                        </svg>
                    </div>`;
            document.getElementById('properties')
                .insertAdjacentHTML('beforeend', html);
        }
    </script>
</section>
