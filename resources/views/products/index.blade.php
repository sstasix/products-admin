<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('products.index_title') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <table class="w-2/3">
                    <thead>
                    <tr class="uppercase text-left">
                        <th class="p-5 pl-0 font-light text-sm">{{ __('products.article_number') }}</th>
                        <th class="p-5 pl-0 font-light text-sm">{{ __('products.name') }}</th>
                        <th class="p-5 pl-0 font-light text-sm">{{ __('products.status') }}</th>
                        <th class="p-5 pl-0 font-light text-sm"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($products as $product)
                        <tr>
                            <td class="p-3 pl-0 text-sm">{{ $product->article_number }}</td>
                            <td class="p-3 pl-0 text-sm">{{ $product->name }}</td>
                            <td class="p-3 pl-0 text-sm">{{ $product->status->getLabelText() }}</td>
                            <td class="p-3 pl-0 ">
                                <a href="{{ route('products.edit', $product) }}">
                                    <svg class="h-6 w-6 text-blue-500 inline-flex"
                                         viewBox="0 0 24 24"
                                         stroke-width="1"
                                         stroke="currentColor"
                                         fill="none"
                                         stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z"/>
                                        <path d="M9 7 h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3" />
                                        <path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3" />
                                        <line x1="16" y1="5" x2="19" y2="8" />
                                    </svg>
                                </a>
                                <form action="{{ route('products.destroy', $product) }}"
                                      method="post"
                                      onsubmit="return confirm('{{ __('destroy_confirm_message') }}')"
                                      class="inline"
                                    >
                                    @csrf
                                    @method('DELETE')
                                    <label>
                                        <input type="submit" class="hidden">
                                        <svg class="h-6 w-6 text-red-500 inline-flex"
                                             width="24"
                                             height="24"
                                             viewBox="0 0 24 24"
                                             stroke-width="1"
                                             stroke="currentColor"
                                             fill="none"
                                             stroke-linecap="round"
                                             stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"/>
                                            <line x1="4" y1="7" x2="20" y2="7" />
                                            <line x1="10" y1="11" x2="10" y2="17" />
                                            <line x1="14" y1="11" x2="14" y2="17" />
                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                        </svg>
                                    </label>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="" colspan="4">{{ __('products.empty_table_message') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>

</x-app-layout>
