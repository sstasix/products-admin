<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\User;
use App\Notifications\ProductCreatedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class SendNotificationOnCreateProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        public Product $product
    ) {
    }

    public function handle(): void
    {
        try {
            $adminEmail = head(config('products.admins'));
            $user = User::where('email', $adminEmail)->first();

            Notification::send($user, new ProductCreatedNotification($this->product));
        } catch (\Exception) {
            info('no admin user for notification');
        }
    }
}
