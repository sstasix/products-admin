<?php

namespace App\Models;

use App\Enums\ProductStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'article_number',
        'status',
        'data',
    ];

    protected $casts = [
        'status' => ProductStatus::class,
        'data' => 'array',
    ];

    protected $perPage = 10;

    public function scopeAvailable(Builder $query): void
    {
        $query->where('status', 'available');
    }
}
