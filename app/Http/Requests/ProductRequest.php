<?php

namespace App\Http\Requests;

use App\Enums\ProductStatus;
use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class ProductRequest extends FormRequest
{
    public function rules(): array
    {
        $rules = [
            'name' => ['required', 'string', 'min:10'],
            'status' => [new Enum(ProductStatus::class)],
            'data' => 'nullable|array',
        ];

        // При создании или для админа при редактировании доступно изменение артикула
        if (request()->routeIs('products.store') || auth()->user()->is_admin) {
            $rules['article_number'] = [
                'required',
                'string',
                'alpha_num:ascii',
                Rule::unique(Product::class, 'article_number')->ignore($this->product),
            ];
        }

        return $rules;
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'data' => empty($this->properties) ? null : array_combine($this->properties, $this->values),
        ]);
    }
}
