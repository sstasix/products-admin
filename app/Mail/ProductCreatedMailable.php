<?php

namespace App\Mail;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ProductCreatedMailable extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public Product $product
    ) {
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address(
                env('MAIL_FROM_ADDRESS', 'hello@example.com'),
                env('MAIL_FROM_NAME', 'Example')
            ),
            subject: __('products.mail_product_created_subject'),
        );
    }

    public function content(): Content
    {
        return new Content(
            view: 'emails.products.created',
            with: [
                'productName' => $this->product->name,
            ],
        );
    }
}
