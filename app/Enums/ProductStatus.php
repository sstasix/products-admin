<?php

namespace App\Enums;

enum ProductStatus: string
{
    case AVAILABLE = 'available';
    case UNAVAILABLE = 'unavailable';

    public function getLabelText(): string
    {
        return match ($this) {
            self::AVAILABLE => 'Доступен',
            self::UNAVAILABLE => 'Недоступен',
        };
    }
}
