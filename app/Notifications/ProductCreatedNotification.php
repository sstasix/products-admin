<?php

namespace App\Notifications;

use App\Mail\ProductCreatedMailable;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(
        public Product $product
    ) {
    }

    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    public function toMail(object $notifiable): ProductCreatedMailable
    {
        return (new ProductCreatedMailable($this->product))
            ->to(config('products.notify_emails'));
    }

}
